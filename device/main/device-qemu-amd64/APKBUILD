# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Minecrell <minecrell@minecrell.net>
# Co-Maintainer: Daniele Debernardi <drebrez@gmail.com>
pkgname=device-qemu-amd64
pkgver=2.2
pkgrel=0
pkgdesc="Simulated device in QEMU (x86_64)"
url="https://postmarketos.org"
arch="x86_64"
license="MIT"
depends="postmarketos-base mesa-dri-gallium"
makedepends="devicepkg-dev"
subpackages="
	$pkgname-weston
	$pkgname-xfce4
	$pkgname-kernel-virt:kernel_virt
	$pkgname-kernel-lts:kernel_lts
"

source="deviceinfo weston.ini"
options="!check !archcheck"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

weston() {
	install_if="$pkgname weston"
	install -Dm644 "$srcdir"/weston.ini \
		"$subpkgdir"/etc/xdg/weston/weston.ini
}

xfce4() {
	install_if="$pkgname postmarketos-ui-xfce4"
	install="$subpkgname.post-install"
	mkdir "$subpkgdir"
}

kernel_virt() {
	pkgdesc="Alpine Virt kernel (minimal, no audio)"
	depends="linux-virt"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_lts() {
	pkgdesc="Alpine LTS kernel"
	depends="linux-lts linux-firmware-none"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

sha512sums="994ab3e870f6374aa56d4f4347ce10a39ba66117f6a054ca735b9b038953a00df0e5f073ba87beac458b5ba7e262e0e89d470fec486f62bc899a1f41e517e0cb  deviceinfo
47b27c7572b8737988488f7eb23b9e68f9a944e22baafe1c78355d2514a2554cf41d99b29fca12238eb13a6f5d53f00ca89b94e534e8461ebab72256dcf0f142  weston.ini"
